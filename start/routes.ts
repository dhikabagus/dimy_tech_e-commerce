/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

//venues
Route.resource('api/v1/customers', 'CustomersController').except(['create','edit'])
Route.resource('api/v1/orders', 'OrdersController').except(['create','edit','store'])
Route.resource('api/v1/paymentMethods', 'PaymentMethodsController').except(['create','edit'])
Route.resource('api/v1/products', 'ProductsController').except(['create','edit'])

//Do a transaction
//JSON DATA EXAMPLE
// {
//     "products":[
//         {
//             "id":2,
//             "amount":1
//         },
//          {
//             "id":1,
//             "amount":2
//         }
//     ],
//     "payment_method_id":1
// }
Route.post('orders/:customerId', 'OrdersController.store').prefix('api/v1/')
