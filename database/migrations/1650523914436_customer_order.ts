import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class CustomerOrders extends BaseSchema {
  protected tableName = 'customer_order'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table
        .integer('customer_id')
        .unsigned()
        .references('customers.id')
        .onDelete('CASCADE')
      table
        .integer('order_id')
        .unsigned()
        .references('orders.id')
        .onDelete('CASCADE')
      table.timestamps(true, true)
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
