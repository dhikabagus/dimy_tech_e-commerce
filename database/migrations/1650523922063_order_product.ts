import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class OrderProducts extends BaseSchema {
  protected tableName = 'order_product'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table
        .integer('order_id')
        .unsigned()
        .references('orders.id')
        .onDelete('CASCADE')
      table
        .integer('product_id')
        .unsigned()
        .references('products.id')
        .onDelete('CASCADE')
      table.integer('amount')
      table.timestamps(true, true)
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
