import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class CustomerAddresses extends BaseSchema {
  protected tableName = 'customer_addresses'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table
        .integer('customer_id')
        .unsigned()
        .references('customers.id')
        .onDelete('CASCADE')
      table.text('address')
      table.timestamps(true, true)
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
