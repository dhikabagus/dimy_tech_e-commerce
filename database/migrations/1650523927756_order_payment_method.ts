import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class OrderPayments extends BaseSchema {
  protected tableName = 'order_payment_method'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table
        .integer('order_id')
        .unsigned()
        .references('orders.id')
        .onDelete('CASCADE')
      table
        .integer('payment_method_id')
        .unsigned()
        .references('payment_methods.id')
        .onDelete('CASCADE')
      table.timestamps(true, true)
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
