import { DateTime } from 'luxon'
import { BaseModel, column, ManyToMany, manyToMany } from '@ioc:Adonis/Lucid/Orm'
import Customer from './Customer'
import PaymentMethod from './PaymentMethod'
import Product from './Product'

export default class Order extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public customerAddress: string

  @column.dateTime({ autoCreate: true })
  public orderDate: DateTime

  @column()
  public totalPrice: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @manyToMany(()=>Customer)
  public customers: ManyToMany<typeof Customer>

  @manyToMany(()=>PaymentMethod)
  public paymentMethods: ManyToMany<typeof PaymentMethod>

  @manyToMany(()=>Product)
  public products: ManyToMany<typeof Product>
}
