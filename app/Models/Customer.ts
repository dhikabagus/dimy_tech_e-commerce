import { DateTime } from 'luxon'
import { BaseModel, column, HasOne, hasOne, manyToMany, ManyToMany } from '@ioc:Adonis/Lucid/Orm'
import CustomerAddress from './CustomerAddress'
import Order from './Order'

export default class Customer extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public customerName: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasOne(() => CustomerAddress,{
    foreignKey: 'customerId'
  })
  public customerAddress: HasOne<typeof CustomerAddress>

  @manyToMany(()=>Order)
  public schedule: ManyToMany<typeof Order>
}
