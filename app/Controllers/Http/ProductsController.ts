import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import ProductValidator from 'App/Validators/ProductValidator'
import Product from 'App/Models/Product'

export default class ProductsController {

    public async index({ response }: HttpContextContract) {
        const products = await Product.all()
        response.ok({ message: "Seluruh data berhasil didapatkan", data: products })
    }

    public async show({ params, response }: HttpContextContract) {
        let id = params.id
        const product = await Product
            .query() // 👈now have access to all query builder methods
            .where('id', id)
            .orWhereNull('id')
            .firstOrFail()
        // const product = await Product.find(id)
        return response.status(200).json({ message: "Data berhasil didapatkan", data: product })
    }

    public async store({ request, response }: HttpContextContract) {
        try {
            await request.validate(ProductValidator)
            const product = new Product()
            await product.fill(request.body()).save()
            return response.ok({ message: "Data berhasil ditambahkan", data: product })
        } catch (err) {
            return response.badRequest({ error: err.messages })
        }
    }

    public async update({ params, request, response }: HttpContextContract) {
        try {
            const product = await Product.findOrFail(params.id)
            await request.validate(ProductValidator)
            let updatedData = request.body()
            await product
                .merge(updatedData)
                .save()
            return response.ok({ message: "Data berhasil diubah", data: request.body() })
        } catch (err) {
            return response.badRequest({ error: err.messages })
        }
    }

    public async destroy({ params, response }: HttpContextContract) {
        try {
            let id = params.id
            const product = await Product.findOrFail(id)
            await product.delete()
            return response.ok({ message: "Data berhasil dihapus", data: id })
        } catch (err) {
            return response.badRequest({ error: err.messages })
        }
    }
}
