import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import OrderValidator from 'App/Validators/OrderValidator'
import Order from 'App/Models/Order'
import Customer from 'App/Models/Customer'
import CustomerAddress from 'App/Models/CustomerAddress'
import PaymentMethod from 'App/Models/PaymentMethod'
import Product from 'App/Models/Product'

export default class OrdersController {

    public async index({ response }: HttpContextContract) {
        const orders = await Order.all()
        response.ok({ message: "Seluruh data berhasil didapatkan", data: orders })
    }

    public async show({ params, response }: HttpContextContract) {
        let id = params.id
        const order = await Order
            .query() // 👈now have access to all query builder methods
            .where('id', id)
            .orWhereNull('id')
            .firstOrFail()
        return response.status(200).json({ message: "Data berhasil didapatkan", data: order })
    }

    //Purchase multiple products
    public async store({ request, response, params }: HttpContextContract) {
        try {
            await request.validate(OrderValidator)
            const cart = request.body()
            let productIds:number[] = [];
            const newOrder:any = new Order()
            //Get all product ids from cart
            for (let i = 0; i < cart.products.length; i++) {
                productIds[i] = cart.products[i].id
            }
            productIds.sort()

            //get all products price
            const products = await Product.query().select('price').whereIn('id', productIds).orderBy('id')
            
            //count total price
            let total_price = 0;
            for(let i = 0;i<products.length;i++){
                total_price = (products[i].price * cart.products[i].amount) + total_price
            }
            newOrder.totalPrice = total_price;

            //get customer address data and put address to new order
            const currentCustomerAddress = await CustomerAddress.find(params.customerId)
            newOrder.customerAddress = currentCustomerAddress?.address

            //create new order data
            await newOrder.fill(newOrder).save()

            //create new order customer relation
            const currentCustomer:any = await Customer.find(params.customerId)
            await newOrder.related('customers').attach([currentCustomer.id])

            //create new order payment relation
            const paymentMethod:any = await PaymentMethod.find(cart.payment_method_id)
            await newOrder.related('paymentMethods').attach([paymentMethod.id])

            //create new order product relation
            await newOrder.related('products').attach(productIds)

            return response.ok({ "message": "Barang berhasil dibeli","new purchase data":newOrder})
        } catch (err) {
            return response.badRequest({ error: err.message })
        }
    }

    public async update({ params, request, response }: HttpContextContract) {
        try {
            const order = await Order.findOrFail(params.id)
            await request.validate(OrderValidator)
            let updatedData = request.body()
            await order
                .merge(updatedData)
                .save()
            return response.ok({ message: "Data berhasil diubah", data: request.body() })
        } catch (err) {
            return response.badRequest({ error: err.messages })
        }
    }

    public async destroy({ params, response }: HttpContextContract) {
        try {
            let id = params.id
            const order = await Order.findOrFail(id)
            await order.delete()
            return response.ok({ message: "Data berhasil dihapus", data: id })
        } catch (err) {
            return response.badRequest({ error: err.messages })
        }
    }
}
