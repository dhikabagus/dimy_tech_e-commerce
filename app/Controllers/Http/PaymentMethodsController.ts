import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import PaymentMethodValidator from 'App/Validators/PaymentMethodValidator'
import PaymentMethod from 'App/Models/PaymentMethod'

export default class PaymentMethodsController {

    public async index({ response }: HttpContextContract) {
        const paymentMethods = await PaymentMethod.all()
        response.ok({ message: "Seluruh data berhasil didapatkan", data: paymentMethods })
    }

    public async show({ params, response }: HttpContextContract) {
        let id = params.id
        const paymentMethod = await PaymentMethod
            .query() // 👈now have access to all query builder methods
            .where('id', id)
            .orWhereNull('id')
            .firstOrFail()
        // const paymentMethod = await PaymentMethod.find(id)
        return response.status(200).json({ message: "Data berhasil didapatkan", data: paymentMethod })
    }

    public async store({ request, response }: HttpContextContract) {
        try {
            await request.validate(PaymentMethodValidator)
            const paymentMethod = new PaymentMethod()
            await paymentMethod.fill(request.body()).save()
            return response.ok({ message: "Data berhasil ditambahkan", data: paymentMethod })
        } catch (err) {
            return response.badRequest({ error: err.messages })
        }
    }

    public async update({ params, request, response }: HttpContextContract) {
        try {
            const paymentMethod = await PaymentMethod.findOrFail(params.id)
            await request.validate(PaymentMethodValidator)
            let updatedData = request.body()
            await paymentMethod
                .merge(updatedData)
                .save()
            return response.ok({ message: "Data berhasil diubah", data: request.body() })
        } catch (err) {
            return response.badRequest({ error: err.messages })
        }
    }

    public async destroy({ params, response }: HttpContextContract) {
        try {
            let id = params.id
            const paymentMethod = await PaymentMethod.findOrFail(id)
            await paymentMethod.delete()
            return response.ok({ message: "Data berhasil dihapus", data: id })
        } catch (err) {
            return response.badRequest({ error: err.messages })
        }
    }
}
