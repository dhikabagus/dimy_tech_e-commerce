import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CustomerValidator from 'App/Validators/CustomerValidator'
import Customer from 'App/Models/Customer'
import CustomerAddress from 'App/Models/CustomerAddress'

export default class CustomersController {

    public async index({ response }: HttpContextContract) {
        const customers = await Customer.all()
        response.ok({ message: "Seluruh data berhasil didapatkan", data: customers })
    }

    public async show({ params, response }: HttpContextContract) {
        let id = params.id
        const customer = await Customer
            .query() // 👈now have access to all query builder methods
            .where('id', id)
            .orWhereNull('id')
            .firstOrFail()
        // const customer = await Customer.find(id)
        return response.status(200).json({ message: "Data berhasil didapatkan", data: customer })
    }

    public async store({ request, response }: HttpContextContract) {
        try {
            await request.validate(CustomerValidator)
            const {customer_name, address} = request.body()
            
            const customer = new Customer() 
            customer.customerName = customer_name
            await customer.fill(customer).save()

            const customerAddress = new CustomerAddress()
            customerAddress.address = address
            customerAddress.customerId = customer.id

            const newCustomerAddress = await customer?.related('customerAddress').save(customerAddress);

            return response.ok({ message: "Data berhasil ditambahkan", data: customer })
        } catch (err) {
            return response.badRequest({ error: err.messages })
        }
    }

    public async update({ params, request, response }: HttpContextContract) {
        try {
            const customer = await Customer.findOrFail(params.id)
            await request.validate(CustomerValidator)
            let updatedData = request.body()
            await customer
                .merge(updatedData)
                .save()
            return response.ok({ message: "Data berhasil diubah", data: request.body() })
        } catch (err) {
            return response.badRequest({ error: err.messages })
        }
    }

    public async destroy({ params, response }: HttpContextContract) {
        try {
            let id = params.id
            const customer = await Customer.findOrFail(id)
            await customer.delete()
            return response.ok({ message: "Data berhasil dihapus", data: id })
        } catch (err) {
            return response.badRequest({ error: err.messages })
        }
    }
}
